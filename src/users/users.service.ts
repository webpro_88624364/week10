import { Injectable } from '@nestjs/common';
import { NotFoundException } from '@nestjs/common/exceptions';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';

let users: User[] = [
  { id: 1, login: 'admin', name: 'Adminstrator', password: 'pass@1234' },
  { id: 2, login: 'admin1', name: 'Usre1', password: 'pass@1234' },
  { id: 3, login: 'admin2', name: 'Usre2', password: 'pass@1234' },
];
let lastUserId = 4;
@Injectable()
export class UsersService {
  create(createUserDto: CreateUserDto) {
    const newUser: User = {
      id: lastUserId++,
      ...createUserDto, //login,name,password
    };
    users.push(newUser);
    return newUser;
  }

  findAll() {
    return users;
  }

  findOne(id: number) {
    const index = users.findIndex((user) => {
      return user.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return users[index];
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    const index = users.findIndex((user) => {
      return user.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    // console.log('user'+JSON.stringtify(users[index]));
    // console.log('update'+JSON.stringtify(updateUserDto));
    const updateUser: User = {
      ...users[index],
      ...updateUserDto,
    };
    users[index] = updateUser;
    return updateUser;
  }

  remove(id: number) {
    const index = users.findIndex((user) => {
      return user.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deleteUser = users[index];
    users.splice(index, 1);
    return deleteUser;
  }
  reset() {
    users = [
      { id: 1, login: 'admin', name: 'Adminstrator', password: 'pass@1234' },
      { id: 2, login: 'admin1', name: 'Usre1', password: 'pass@1234' },
      { id: 3, login: 'admin2', name: 'Usre2', password: 'pass@1234' },
    ];
    lastUserId = 4;
    return 'RESET';
  }
}
