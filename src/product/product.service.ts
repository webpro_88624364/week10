import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

let product: Product[] = [
  { id: 1, name: 'Latte', price: 55 },
  { id: 2, name: 'Chiffon', price: 35 },
  { id: 3, name: 'Smoothie', price: 90 },
];
let lastProductId = 4;
@Injectable()
export class ProductService {
  create(createProductDto: CreateProductDto) {
    const newProduct: Product = {
      id: lastProductId++,
      ...createProductDto, //id,name,price
    };
    product.push(newProduct);
    return newProduct;
  }

  findAll() {
    return product;
  }

  findOne(id: number) {
    const index = product.findIndex((products) => {
      return products.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return product[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = product.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }

    const updateProduct: Product = {
      ...product[index],
      ...updateProductDto,
    };
    product[index] = updateProduct;
    return updateProduct;
  }

  remove(id: number) {
    const index = product.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }

    const deleteProduct = product[index];
    product.splice(index, 1);
    return deleteProduct;
  }
  reset() {
    product = [
      { id: 1, name: 'Latte', price: 55 },
      { id: 2, name: 'Chiffon', price: 35 },
      { id: 3, name: 'Smoothie', price: 90 },
    ];
    lastProductId = 4;
    return 'RESET';
  }
}
